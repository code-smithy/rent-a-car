import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { CarouselModule } from '@coreui/angular';

@Component({
  selector: 'app-carousel',
  standalone: true,
  imports: [CarouselModule, CommonModule],
  templateUrl: './carousel.component.html',
  styleUrl: './carousel.component.scss',
})
export class CarouselComponent {
  slides: any[] = [
    {
      id: 1,
      src: 'assets/img/carousel-1.jpg',
      title: 'Rent A Car',
      subtitle: 'Best Rental Cars In Your Location',
    },
    {
      id: 2,
      src: 'assets/img/carousel-2.jpg',
      title: 'Rent A Car',
      subtitle: 'Quality Cars with Unlimited Miles',
    },
  ];
}
