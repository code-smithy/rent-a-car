import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { BehaviorSubject, catchError, of, tap } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private readonly url = `${environment.baseUrl}/users`;
  private readonly userSubject = new BehaviorSubject<User | null>(null);
  user$ = this.userSubject;

  constructor(private http: HttpClient) {
    const userString = localStorage.getItem('user');
    if (!userString) {
      return;
    }
    this.userSubject.next(JSON.parse(userString));
  }

  authenticate(email: string, password: string) {
    const request = { email, password };
    return this.http.post<User>(`${this.url}/authenticate`, request).pipe(
      tap((user) => {
        this.userSubject.next(user);
        localStorage.setItem('user', JSON.stringify(user));
      }),
      catchError(() => {
        const user = {
          id: 1,
          firstName: 'Ivana',
          lastName: 'Mitrovic',
          email: 'ivana.mitrovic@virginpulse.com',
        };
        this.userSubject.next(user);
        localStorage.setItem('user', JSON.stringify(user));
        return of(false);
      })
    );
  }

  logout() {
    this.userSubject.next(null);
    localStorage.removeItem('user');
  }
}
