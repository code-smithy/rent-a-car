import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { BehaviorSubject, catchError, of } from 'rxjs';
import { Car } from '../models/car.model';

@Injectable({
  providedIn: 'root',
})
export class CarService {
  private readonly url = `${environment.baseUrl}/cars`;
  cars = [
    {
      id: 1,
      brand: 'Audi',
      model: 'A4',
      price: 100,
      color: 'Black',
      registrationPlate: 'A84-E-345',
    },
    {
      id: 1,
      brand: 'Audi',
      model: 'A4',
      price: 100,
      color: 'Black',
      registrationPlate: 'A84-E-345',
    },
    {
      id: 1,
      brand: 'Audi',
      model: 'A4',
      price: 100,
      color: 'Black',
      registrationPlate: 'A84-E-345',
    },
    {
      id: 1,
      brand: 'Audi',
      model: 'A4',
      price: 100,
      color: 'Black',
      registrationPlate: 'A84-E-345',
    },
    {
      id: 1,
      brand: 'Audi',
      model: 'A4',
      price: 100,
      color: 'Black',
      registrationPlate: 'A84-E-345',
    },
    {
      id: 1,
      brand: 'Audi',
      model: 'A4',
      price: 100,
      color: 'Black',
      registrationPlate: 'A84-E-345',
    },
  ];

  private readonly carsSubject = new BehaviorSubject<Car[]>([]);
  readonly cars$ = this.carsSubject.asObservable();

  constructor(private readonly http: HttpClient) {}

  getAll() {
    this.http
      .get(this.url)
      .pipe(
        catchError(() => {
          this.carsSubject.next(this.cars);
          return of(false);
        })
      )
      .subscribe();
  }
}
