export interface Car {
  id: number;
  brand: string;
  model: string;
  price: number;
  color: string;
  registrationPlate: string;
}
