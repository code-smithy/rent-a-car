import { Component } from '@angular/core';
import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { UserService } from '../../services/user.service';
import { lastValueFrom } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [ReactiveFormsModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss',
})
export class LoginComponent {
  form = new FormGroup({
    email: new FormControl<string>('', [Validators.required, Validators.email]),
    password: new FormControl<string>('', Validators.required),
  });

  constructor(
    private readonly userService: UserService,
    private readonly router: Router
  ) {}

  async onSubmit() {
    const { email, password } = this.form.value;
    if (!email || !password) {
      return;
    }
    await lastValueFrom(this.userService.authenticate(email, password));
    this.router.navigateByUrl('/');
  }
}
