import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    loadComponent: () =>
      import('./pages/home/home.component').then((mod) => mod.HomeComponent),
  },
  {
    path: 'login',
    loadComponent: () =>
      import('./pages/login/login.component').then((mod) => mod.LoginComponent),
  },
  {
    path: 'logout',
    loadComponent: () =>
      import('./pages/logout/logout.component').then(
        (mod) => mod.LogoutComponent
      ),
  },
  {
    path: 'cars',
    loadComponent: () =>
      import('./pages/cars/cars.component').then((mod) => mod.CarsComponent),
  },
  {
    path: '**',
    loadComponent: () =>
      import('./pages/home/home.component').then((mod) => mod.HomeComponent),
  },
];
